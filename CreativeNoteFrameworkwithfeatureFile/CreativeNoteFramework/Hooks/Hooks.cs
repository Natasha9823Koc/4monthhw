﻿using CreativeNoteFramework.Driver;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CreativeNoteFramework.Hooks
{
    [Binding]
    public sealed class Hooks
    {
        // For additional details on SpecFlow hooks see http://go.specflow.org/doc-hooks
        private readonly ScenarioContext _scenarioContext;
        public Hooks(ScenarioContext scenarioContext) => _scenarioContext = scenarioContext;
        [BeforeScenario]
        public void BeforeScenario()
        {
            SeleniumWebDriver seleniumWebDriver = new SeleniumWebDriver(_scenarioContext);
            _scenarioContext.Set(seleniumWebDriver, "SeleniumDriver");
        }

        [AfterScenario]
        public void AfterScenario()
        {
            _scenarioContext.Get<IWebDriver>("WebDriver").Quit();
        }
    }
}
