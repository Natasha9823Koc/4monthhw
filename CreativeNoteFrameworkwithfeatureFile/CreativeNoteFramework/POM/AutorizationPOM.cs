﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreativeNoteFramework.POM
{
    internal static class AutorizationPOM
    {
        internal static IWebDriver webdriver;
        internal static By _logInInputButton = By.XPath("//input[@type='text']");
        internal static By _passwordInputButton = By.XPath("//input[@type='password']");
        internal static By _logInButton = By.XPath("//*[@id='root']/div[2]/button");
        internal static By _notValidErrorMassage = By.CssSelector("/html/body/div[1]/div[3]/div/div[2]/div[1]/div[2]");

        internal static string GetUrl() => webdriver.Url;
        internal static IWebElement GetLoginField() => webdriver.FindElement(_logInInputButton);
        internal static IWebElement GetPasswordField() => webdriver.FindElement(_passwordInputButton);
        internal static IWebElement GetLoginButton() => webdriver.FindElement(_logInButton);
        internal static string GetNotValidEmailError() => webdriver.FindElement(_notValidErrorMassage).Text;
        internal static string GetNotValidPasswordError() => webdriver.FindElement(_notValidErrorMassage).Text;


        internal static void Authorization(string login, string password)
        {
            var loginField = GetLoginField();
            loginField.SendKeys(login);
            var passwordField = GetPasswordField();
            passwordField.SendKeys(password);
            var loginButton = GetLoginButton();
            loginButton.Click();
        }
    }
}
