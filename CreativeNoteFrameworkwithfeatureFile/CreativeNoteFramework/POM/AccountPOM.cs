﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreativeNoteFramework.POM
{
    internal static class AccountPOM
    {

        internal static IWebDriver webdriver;
        internal static By _additionalInfoButton = By.Id("change-info");
        internal static By _changeAdditionalInfoButton = By.XPath("//img[@src= 'assets/img/crest.png']");
        internal static By _nameInputButton = By.CssSelector(" div.sc-lmoMya.fBRqpo > span:nth-child(1) > div > input");
        internal static By _lastNameInputButton = By.CssSelector(" div.sc-lmoMya.fBRqpo > span:nth-child(2) > div > input");
        internal static By _cityInputButton = By.CssSelector(" div.sc-lmoMya.fBRqpo > span:nth-child(3) > div > input");
        internal static By _ageInputButton = By.CssSelector(" div.sc-lmoMya.fBRqpo > span:nth-child(4) > div > input");
        internal static By _addFileButton = By.XPath("//label[@for='upload']");
        internal static By _submitButton = By.CssSelector(" div.sc-dmlqKv.hSQiWD > div > button");
        internal static By _changePasswordButton = By.Id("change-password");
        internal static By _currentPasswordInputButton = By.XPath("//*[@id='root']/div[2]/div[2]/div/div[1]/input");
        internal static By _newPasswordInputButton = By.XPath("//*[@id='root']/div[2]/div[2]/div/div[2]/input");
        internal static By _nameField = By.CssSelector(" div.sc-lmoMya.fBRqpo > span:nth-child(1) > p:nth-child(2)");
        internal static By _lastnameField = By.CssSelector("div.sc-lmoMya.fBRqpo > span:nth-child(2) > p:nth-child(2)");
        internal static By _cityField = By.CssSelector("div.sc-lmoMya.fBRqpo > span:nth-child(3) > p:nth-child(2)");
        internal static By _ageField = By.CssSelector("div.sc-lmoMya.fBRqpo > span:nth-child(4) > p:nth-child(2)");
        internal static By _successmessage = By.XPath("/html/body/div[1]/div[3]/div/div/div[1]/div[2]");



        internal static string GetUrl() => webdriver.Url;
        internal static IWebElement GetAdditionalInfoButton() => webdriver.FindElement(_additionalInfoButton);
        internal static IWebElement GetChangeAdditionalInfoButton() => webdriver.FindElement(_changeAdditionalInfoButton);
        internal static IWebElement GetNameField() => webdriver.FindElement(_nameInputButton);
        internal static IWebElement GetLastNameField() => webdriver.FindElement(_lastNameInputButton);
        internal static IWebElement GetCityField() => webdriver.FindElement(_cityInputButton);
        internal static IWebElement GetAgeField() => webdriver.FindElement(_ageInputButton);
        internal static IWebElement GetFileUploud() => webdriver.FindElement(_addFileButton);
        internal static IWebElement GetChangePasswordButton() => webdriver.FindElement(_changePasswordButton);

        internal static IWebElement GetCurrentPasswordField() => webdriver.FindElement(_currentPasswordInputButton);
        internal static IWebElement GetNewPasswordField() => webdriver.FindElement(_newPasswordInputButton);

        internal static IWebElement GetSubmitButton() => webdriver.FindElement(_submitButton);
        internal static string GetName() => webdriver.FindElement(_nameField).Text;
        internal static string GetLastName() => webdriver.FindElement(_lastnameField).Text;
        internal static string GetCity() => webdriver.FindElement(_cityField).Text;
        internal static string GetAge() => webdriver.FindElement(_ageField).Text;
        internal static string GetMessage() => webdriver.FindElement(_successmessage).Text;






        internal static void ChangeAdditionalData(string newname, string newlastname, string newcity, string newage)
        {
            //var additionalInfoButton = GetAdditionalInfoButton();
            //additionalInfoButton.Click();
            //var changeAdditionalInfo = GetChangeAdditionalInfoButton();
            //changeAdditionalInfo.Click();
            var name = GetNameField();
            name.Clear();
            name.SendKeys(newname);
            var lastname = GetLastNameField();
            lastname.Clear();
            lastname.SendKeys(newlastname); ;
            var city = GetCityField();
            city.Clear();
            city.SendKeys(newcity);
            var age = GetAgeField();
            age.Clear();
            age.SendKeys(newage);
            //var submit = GetSubmitButton();
            //submit.Click();
        }
        internal static void UploadPhoto(string path)
        {
            var fileUpload = GetFileUploud();
            fileUpload.Click();
            fileUpload.SendKeys(path);
            var submit = GetSubmitButton();
        }
        internal static void ChangePassword(string currentpass, string newpass)
        {
            //var changePassword = GetChangePasswordButton();
            //changePassword.Click();
            var currentPassword = GetCurrentPasswordField();
            currentPassword.SendKeys(currentpass);
            var newPassword = GetNewPasswordField();
            newPassword.SendKeys(newpass);
            //var submit = GetSubmitButton();
            //submit.Click();
        }
    }
}
