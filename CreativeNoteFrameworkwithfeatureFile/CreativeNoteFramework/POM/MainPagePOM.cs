﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreativeNoteFramework.POM
{
    internal static class MainPagePOM
    {

        internal static IWebDriver webdriver;
        internal static By _accountButton = By.XPath("//*[@id='root']/div[1]/div/div/div/div/img");
        internal static By _logOutButton = By.CssSelector("#root > div.sc-iqHYmW.eTxQfa > div > div > div > span > img");


        internal static IWebElement GetAccountButton() => webdriver.FindElement(_accountButton);
        internal static IWebElement GetLogOutButton() => webdriver.FindElement(_logOutButton);

    }
}
