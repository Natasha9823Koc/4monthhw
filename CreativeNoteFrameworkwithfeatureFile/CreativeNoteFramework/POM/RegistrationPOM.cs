﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreativeNoteFramework.POM
{
    internal static class RegistrationPOM
    {
        internal static IWebDriver webdriver;
        internal static By _logInInputButton = By.XPath("//input[@type='text']");
        internal static By _passwordInputButton = By.XPath("//input[@placeholder='Enter your password...']");
        internal static By _passwordConfirmInputButton = By.XPath("//input[@placeholder='Confirm password...']");
        internal static By _signUpButton = By.CssSelector("#root > div.sc-fubCzh.eYBUqz > button");
        internal static By _notValidEmailErrorMessage = By.CssSelector("div[role='alert'] > div:nth-child(2)");
        internal static By _notValidPasswordErrorMessage = By.CssSelector("div.Toastify__toast-body > div:nth-child(2)");
        internal static By _notValidPasswordConfirmErrorMessage = By.CssSelector("div.Toastify__toast-body > div:nth-child(2)");



        internal static string GetUrl() => webdriver.Url;
        internal static IWebElement GetLoginField() => webdriver.FindElement(_logInInputButton);
        internal static IWebElement GetPasswordField() => webdriver.FindElement(_passwordInputButton);
        internal static IWebElement GetPasswordConfirmField() => webdriver.FindElement(_passwordConfirmInputButton);
        internal static IWebElement GetSignUpButton() => webdriver.FindElement(_signUpButton);

        internal static string GetNotValidEmailError() => webdriver.FindElement(_notValidEmailErrorMessage).Text;

        internal static string GetNotValidPasswordError() => webdriver.FindElement(_notValidPasswordErrorMessage).Text;

        internal static string GetNotValidPasswordConfirmError() => webdriver.FindElement(_notValidPasswordConfirmErrorMessage).Text;
        internal static void Registration(string login, string passsword, string passwordconfirm)
        {
            var loginField = GetLoginField();
            loginField.SendKeys(login);
            var passwordField = GetPasswordField();
            passwordField.SendKeys(passsword);
            var passwordConfirmField = GetPasswordConfirmField();
            passwordConfirmField.SendKeys(passwordconfirm);
            var signup = GetSignUpButton();
            signup.Click();
        }
    }
}
