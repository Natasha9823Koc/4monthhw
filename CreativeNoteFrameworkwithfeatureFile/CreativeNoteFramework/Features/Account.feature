﻿@HW_19m @Accauntmodule
Feature: Accaunt
	In order to to update information in the application
	As a CreativeNotes app user
	I want to change my profile data

Background: 
Given Browser is opened
And User is registated in the application
	| User    | Password   |
	| Natasha | qwert12345 |
And User is registated in the application
	| Password   | User |
	| qwert12345 | Natasha |
And User is authorized in the application
	| User    | Password   |
	| Natasha | qwert12345 |

@p1 @smoke @PositiveScenario
Scenario Outline: Change addirional information with valid data
Given The page for editing additional information is opened
And Edit additional information button is pressed
When I fill the field with valid information
	| Name   | Lastname   | City   | Age   |
	| <name> | <lastname> | <city> | <age> |
And I click on the <'submit'> button
Then the new value is displayed in the field
	| Name   | Lastname   | City   | Age   |
	| <name> | <lastname> | <city> | <age> |
Examples: 
	| name     | lastname     | city         | age |
	| Test     | Test         | Test         | 18  |
	| Mynameis | Mylastnameis | Mycityistest | 100 |

@p1 @smoke @PositiveScenario
Scenario Outline: Change password with valid data
Given The page for editing password is opened
When I fill the fields with not valid information
	| Current password   | New password  |
	| <current password> | <new password> |
And I click on the '<submit>' button
Then 'Password changed successfully' message is displayed on the editing page
Examples:
	| current password | new password |
	| qwert12345       | test1234     |
	| qwert12345       | Test12345     |

@p1 @NegativeScenario
Scenario Outline: No possible change password with not valid data
Given The page for editing password is opened
When I fill the fields with not valid information
	| Current password   | New password  |
	| <current password> | <new password> |
And I click on the <submit> button
Then '<notification message>' message is displayed on the editing page
When I log out
And I log in with <'current password'> and <'current login'>
Then Autorization was  not successed
And 'Invalid current password' message is displayed on the main page 
Examples: 
	| current login | current password | new password | notification message                                                                                                                 |
	| Ivan          | qwert12345       | qwert        | The new password must be from 4 to 12 characters long, consist only of letters and numbers, be sure to contain 1 letter and 1 number |
	| Ivan          |qwert            | qwert12345   | Invalid current password                                                                                                             |
	



