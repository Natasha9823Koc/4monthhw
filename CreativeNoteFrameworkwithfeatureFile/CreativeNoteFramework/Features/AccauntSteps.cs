﻿using CreativeNoteFramework.POM;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;
using TechTalk.SpecFlow;

namespace CreativeNoteFramework.Features
{
    [Binding]
    public class AccauntSteps
    {
        IWebDriver driver;
        [Given(@"Browser is opened")]
        public void GivenBrowserIsOpened()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://3.133.100.46/login");
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }

        [Given(@"User is registated in the application")]
        public void GivenUserIsRegistatedInTheApplication(Table table)
        {
            var login = table.Rows[0]["User"];
            var password = table.Rows[0]["Password"];
            RegistrationPOM.Registration(login, password, password);
        }

        [Given(@"User is authorized in the application")]
        public void GivenUserIsAuthorizedInTheApplication(Table table)
        {
            var login = table.Rows[0]["User"];
            var password = table.Rows[0]["Password"];
            AutorizationPOM.Authorization(login, password);
        }

        [Given(@"The page for editing additional information is opened")]
        public void GivenThePageForEditingAdditionalInformationIsOpened()
        {
            MainPagePOM.GetAccountButton().Click();
            AccountPOM.GetAdditionalInfoButton().Click();

        }

        [Given(@"Edit additional information button is pressed")]
        public void GivenEditAdditionalInformationButtonIsPressed()
        {
            AccountPOM.GetChangeAdditionalInfoButton().Click();
        }

        [Given(@"The page for editing password is opened")]
        public void GivenThePageForEditingPasswordIsOpened()
        {
            AccountPOM.GetChangePasswordButton().Click();
        }

        [When(@"I click on the '(.*)' button")]
        public void WhenIClickOnTheButton(string p0)
        {
            AccountPOM.GetSubmitButton().Click();
        }

        [When(@"I fill the field with valid information")]
        public void WhenIFillTheFieldWithValidInformation(Table table)
        {
            var name = table.Rows[0]["Name"];
            var lastname = table.Rows[0]["Lastname"];
            var city = table.Rows[0]["City"];
            var age = table.Rows[0]["Age"];
            AccountPOM.ChangeAdditionalData(name, lastname, city, age);
        }



        [When(@"I fill the fields with not valid information")]
        public void WhenIFillTheFieldsWithNotValidInformation(Table table)
        {
            var currentPsword = table.Rows[0]["Current password"];
            var newPassword = table.Rows[0]["New password"];
            AccountPOM.ChangePassword(currentPsword, newPassword);

        }



        [Then(@"the new value is displayed in the field")]
        public void ThenTheNewValueIsDisplayedInTheField(Table table)
        {
            var name = table.Rows[0]["Name"];
            var lastname = table.Rows[0]["Lastname"];
            var city = table.Rows[0]["City"];
            var age = table.Rows[0]["Age"];
            var actualName = AccountPOM.GetName();
            var actualLastName = AccountPOM.GetLastName();
            var actualCity = AccountPOM.GetCity();
            var actualAge = AccountPOM.GetAge();

            Assert.AreEqual(actualName, name);
            Assert.AreEqual(actualLastName, lastname);
            Assert.AreEqual(actualCity, city);
            Assert.AreEqual(actualAge, age);
        }



        [Then(@"'(.*)' message is displayed on the editing page")]
        public void ThenMessageIsDisplayedOnTheEditingPage(string message)
        {
            var actualmessage = AccountPOM.GetMessage();
            Assert.AreEqual(message, actualmessage);
        }
        [When(@"I log out")]
        public void WhenILogOut()
        {
            MainPagePOM.GetLogOutButton().Click();
        }

        [When(@"I log in with (.*) and (.*)")]
        public void WhenILogInWithAnd(string password, string login)
        {
            AutorizationPOM.Authorization(login, password);
            Thread.Sleep(3000);
        }

        [Then(@"Autorization was  not successed")]
        public void ThenAutorizationWasNotSuccessed()
        {
            string ExpURL = "http://3.144.199.135/login";
            Assert.AreEqual(ExpURL, AutorizationPOM.GetUrl(), "Sorry...Wrong information");
        }

        [Then(@"'(.*)' message is displayed on the main page")]
        public void ThenMessageIsDisplayedOnTheMainPage(string errormessage)
        {
            var actualError = AutorizationPOM.GetNotValidPasswordError();
            Assert.AreEqual(errormessage, actualError, "Sorry...Wrong information");
        }


    }

}
