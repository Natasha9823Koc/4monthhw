﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace CreativeNoteFramework.Driver
{
   public class SeleniumWebDriver
    {
        private IWebDriver webDriver;
        private readonly ScenarioContext _scenarioContext;
        public SeleniumWebDriver(ScenarioContext scenarioContext) => _scenarioContext = scenarioContext;
        public IWebDriver SetUp()
        {
            var option = new ChromeOptions();
            option.AddArguments("--start-maximized");
            webDriver = new ChromeDriver(option);
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
            _scenarioContext.Set(webDriver, "WebDriver");
            return webDriver;
        }
        
    }
}
