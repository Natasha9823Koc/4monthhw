﻿using Facebook.Features.Registration;
using Facebook.POM;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnitTestProject1.Support;
using OpenQA.Selenium.Support.UI;
using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Facebook.Features
{

    [Binding]
    public class RegistrationSteps/*: Hooks*/
    {
        string _tempMail { get; set; }
        string _firstName { get; set; }
        string _lastName { get; set; }
        int _value { get; set; }

        private string successURL = "https://www.facebook.com/";

        [Given(@"Registration page is open")]
        public void GivenRegistrationPageIsOpen()
        {
            _tempMail = TempMailPOM.GetTempEmail();
            ChromeBrowser.GetDriver().Navigate().GoToUrl("https://uk-ua.facebook.com/");
            RagistrationFacebookPOM.ClickRegistartionButton();
        }

        [Given(@"The data is filled in the registration field")]
        public void GivenTheDataIsFilledInTheRegistrationField(Table table)
        {
            var model = table.CreateInstance<RegistrationModel>();
            _firstName = model.Firstname;
            _lastName = model.Lastname;
            var password = model.Newpassword;
            RagistrationFacebookPOM.SetFirstName(_firstName);
            RagistrationFacebookPOM.SetLastName(_lastName);
            RagistrationFacebookPOM.SetEmail(_tempMail);
            RagistrationFacebookPOM.SetPassword(password);
        }

        [When(@"I choose in field birthday")]
        public void WhenIChooseInFieldBirthday(Table table)
        {
            var model = table.CreateInstance<RegistrationModel>();
            var day = model.Day;
            var month = model.Month;
            int monthValue = 0;
            switch (month)
            {
                case "Jan":
                    monthValue = 1;
                    break;
                case "Feb":
                    monthValue = 2;
                    break;
                case "Mar":
                    monthValue = 3;
                    break;
                case "Apr":
                    monthValue = 4;
                    break;
                case "May":
                    monthValue = 5;
                    break;
                case "Jun":
                    monthValue = 6;
                    break;
                case "Jul":
                    monthValue = 7;
                    break;
                case "Aug":
                    monthValue = 8;
                    break;
                case "Sep":
                    monthValue = 9;
                    break;
                case "Oct":
                    monthValue = 10;
                    break;
                case "Nov":
                    monthValue = 11;
                    break;
                case "Dec":
                    monthValue = 12;
                    break;

            }
            var year = model.Year;
            RagistrationFacebookPOM.ClickAndSetBirthdayDay(day);
            RagistrationFacebookPOM.ClickAndSetBirthdayMonth(monthValue);
            RagistrationFacebookPOM.ClickAndSetBirthdayYear(year);
        }

        [When(@"I choose '(.*)'")]
        public void WhenIChoose(string gender)
        {
            switch (gender)
            {
                case "Male":
                    RagistrationFacebookPOM.SetMaleGender();
                    break;
                case "Female":
                    RagistrationFacebookPOM.SetFemaleGender();
                    break;
                case "Custom":
                    RagistrationFacebookPOM.SetCustomGender();
                    break;
            }
        }

        [When(@"I press the button Sign up to register")]
        public void WhenIPressTheButtonSignUpToRegister()
        {
            RagistrationFacebookPOM.ClickSubmitButton();
        }


        [When(@"I enter the confirmation code that came to my phone")]
        public void WhenIEnterTheConfirmationCodeThatCameToMyPhone()
        {
            RagistrationFacebookPOM.SetCode();
        }


        [When(@"I enter the '(.*)' into the field Gender\(optional\)")]
        public void WhenIEnterTheIntoTheFieldGenderOptional(string description)
        {
            RagistrationFacebookPOM.SetGenderDescription(description);
        }

        [When(@"I choose '(.*)' in field Select your pronoun")]
        public void WhenIChooseInFieldSelectYourPronoun(string pronoun)
        {
            switch (pronoun[0])
            {
                case 'H':
                    _value = 2;
                    break;
                case 'S':
                    _value = 3;
                    break;
                case 'T':
                    _value = 4;
                    break;
            }
            RagistrationFacebookPOM.ClickAndSetPronoun(_value);
        }

        [Then(@"Account created")]
        public void ThenAccountCreated()
        {
            Assert.IsTrue(RagistrationFacebookPOM.ClickOk());
        }

        [Then(@"Open main page of application")]
        public void ThenOpenMainPageOfApplication()
        {
            var expectedName = _firstName + " " + _lastName;
            var actualName = RagistrationFacebookPOM.FindName();
            Assert.AreEqual(expectedName, actualName);
        }

        [Then(@"The Notification is displayed")]
        public void ThenTheNotificationIsDisplayed(Table table)
        {
            var model = table.CreateInstance<RegistrationModel>();
            var expextedNotification = model.Notification;
            var actualNotification = RagistrationFacebookPOM.GetErrorMessage();
            Assert.AreEqual(expextedNotification, actualNotification);
        }
    }
}
