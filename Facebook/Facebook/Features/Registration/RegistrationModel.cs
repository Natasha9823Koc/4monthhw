﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facebook.Features.Registration
{
  public  class RegistrationModel
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string MobileNumberOrEmail { get; set; }
        public string Newpassword { get; set; }
        public string Month { get; set; }
        public int Day { get; set; }
        public int Year { get; set; }
        public string Gender { get; set; }
        public string Notification { get; set; }

    }
}
