﻿@HW_Friday/29.10.2021 @Authorizationmodule
Feature: Authorization
	 In order to use all functions in application Facebook
	 As a user of application 
	 I want to be able to authorizate in application

#@p1 @PositiveScenario @smoke
#Scenario Outline: Authorization with valid data
#	Given The page of authorization is open
#	And The data is filled in the authorization field
#		| Login                    | Password   |
#		| <mobile number or email> | <password> |
#	When I press the button Log in to authorizate
#	Then The home page with user personal data is open
#		| UserName    | UserLastName     |
#		| <user name> | <user last name> |
#Examples:
#		| mobile number or email | password       |
#		| nestea17gmail.com      | Qwert12345     |
#		| +380957247922          | DTEK_Chernev20 |

#@p1 @NegativeScenario
#Scenario Outline: Authorization with not valid data
#	Given The page of authorization is open
#	And The data is filled in the authorization field
#		| Login                     | Password   |
#		| < email or phone number > | <password> |
#	When I press the button Log in to authorizate
#	Then Then The notification is appear on the display
#		| Notification   |
#		| <notification> |
#Examples:
#		| email or phone number | password   | notification                                                                                        |
#		|                       | Qwert12345 | The email or mobile number you entered isn’t connected to an account. Find your account and log in. |
#		| +380957247922         |            | The password you’ve entered is incorrect. Forgot Password?                                          |

#@p2 @Language
#Scenario: Changing interface language in application
#	Given Authorization page is open
#	And English language of interface has been chosen
#	When I press the link 'Русский'
#	Then The interface language of application changed into the russian language
#
#@p2 @Switchmodules
#Scenario: Switch between registration and authorization modules
#	Given Authorization page is open
#	When I press the button 'Sign up for Facebook'
#	Then The registration page is opened
#
#@p2 @Restoration
#Scenario: Restoration password
#	Given Authorization page is open
#	When I press the button 'Forgot password'
#	Then The page with restoration password is opened





