﻿using Facebook.POM;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnitTestProject1.Support;
using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Facebook.Features.Authorization
{
    [Binding]
    public class AuthorizationSteps
    {
        
        [Given(@"The page of authorization is open")]
        public void GivenThePageOfAuthorizationIsOpen()
        {
            ChromeBrowser.GetDriver().Navigate().GoToUrl("https://uk-ua.facebook.com/");
        }
        
        [Given(@"The data is filled in the authorization field")]
        public void GivenTheDataIsFilledInTheAuthorizationField(Table table)
        {
            var model = table.CreateInstance<AuthorizationModel>();
            var email = model.Login;
            AuthorizationPOM.SetEmail(email);
            var password = model.Password;
            AuthorizationPOM.SetPassword(password);
            AuthorizationPOM.ClickLogIn();
        }
        
        [When(@"I press the button Log in to authorizate")]
        public void WhenIPressTheButtonLogInToAuthorizate()
        {
            AuthorizationPOM.ClickLogIn();
        }

        [Then(@"The home page with user personal data is open")]
        public void ThenTheHomePageWithUserPersonalDataIsOpen(Table table)
        {
            var model = table.CreateInstance<AuthorizationModel>();
            var firstName = model.UserName;
            var lastName = model.UserLastName;
            var expectedName = firstName + " " + lastName;
            var actualName = AuthorizationPOM.FindName();
            Assert.AreEqual(expectedName, actualName);
        }

        [Then(@"Then The notification is appear on the display")]
        public void ThenThenTheNotificationIsAppearOnTheDisplay(Table table)
        {
            var model = table.CreateInstance<AuthorizationModel>();
            var expextedNotification = model.Notification;
            var actualNotification = AuthorizationPOM.GetErrorMessage();
            Assert.AreEqual(expextedNotification, actualNotification);
        }

    }
}
