﻿using NUnitTestProject1.Support;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facebook.POM
{
    internal static class AuthorizationPOM
    {

        internal static By _emailOrPhoneNumberInputButton = By.Name("email");
        internal static By _passwordInputButton = By.Name("pass");
        internal static By _login = By.Name("login");
        internal static By _errorMessage = By.XPath("/html/body/div[1]/div[2]/div[1]/div/div[2]/div[2]/form/div/div[1]/div[2]/text()");
        internal static By _accountName = By.CssSelector("#mount_0_0_bM > div > div:nth-child(1) > div > div.rq0escxv.l9j0dhe7.du4w35lb > div > div > div.j83agx80.cbu4d94t.d6urw2fd.dp1hu0rb.l9j0dhe7.du4w35lb > div.rq0escxv.l9j0dhe7.du4w35lb.j83agx80.pfnyh3mw.taijpn5t.gs1a9yip.owycx6da.btwxx1t3.dp1hu0rb.p01isnhg > div > div.rq0escxv.lpgh02oy.du4w35lb.o387gat7.qbu88020.pad24vr5.rirtxc74.dp1hu0rb.fer614ym.ni8dbmo4.stjgntxs.rek2kq2y.be9z9djy.bx45vsiw > div > div > div.j83agx80.cbu4d94t.buofh1pr.l9j0dhe7 > div > div > div.buofh1pr > ul > li > div > a > div.ow4ym5g4.auili1gw.rq0escxv.j83agx80.buofh1pr.g5gj957u.i1fnvgqd.oygrvhab.cxmmr5t8.hcukyx3x.kvgmc6g5.nnctdnn4.hpfvmrgz.qt6c0cv9.jb3vyjys.l9j0dhe7.du4w35lb.bp9cbjyn.btwxx1t3.dflh9lhu.scb9dxdr > div.ow4ym5g4.auili1gw.rq0escxv.j83agx80.buofh1pr.g5gj957u.i1fnvgqd.oygrvhab.cxmmr5t8.hcukyx3x.kvgmc6g5.tgvbjcpo.hpfvmrgz.qt6c0cv9.rz4wbd8a.a8nywdso.jb3vyjys.du4w35lb.bp9cbjyn.btwxx1t3.l9j0dhe7 > div > div > div > div > span > span");

        internal static void SetEmail(string email) =>
            ChromeBrowser.GetDriver().FindElement(_emailOrPhoneNumberInputButton).SendKeys(email);
        internal static void SetPassword(string password) =>
            ChromeBrowser.GetDriver().FindElement(_passwordInputButton).SendKeys(password);
        internal static void ClickLogIn() =>
            ChromeBrowser.GetDriver().FindElement(_login).Click();
        internal static string GetErrorMessage() =>
            ChromeBrowser.GetDriver().FindElement(_errorMessage).Text;
        internal static string FindName() => ChromeBrowser.GetDriver().FindElement(_accountName).Text;



    }
}
