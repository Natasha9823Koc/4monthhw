﻿using Facebook.TempMail;
using NUnitTestProject1.Support;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facebook.POM
{
    internal static class RagistrationFacebookPOM
    {
        private static int _day { get; set; }
        private static int _month { get; set; }
        private static int _year { get; set; }
        private static int _pronoun { get; set; }

        internal static By _createNewAccountButton = By.XPath("/html/body/div[1]/div[2]/div[1]/div/div/div/div[2]/div/div[1]/form/div[5]/a");
        internal static By _firstNameInputButton = By.Name("firstname");
        internal static By _lastNameInputButton = By.Name("lastname");
        internal static By _emailInputButton = By.Name("reg_email__");
        internal static By _reEnterEmailInputButton = By.Name("reg_email_confirmation__");
        internal static By _passwordInputButton = By.Id("password_step_input");
        internal static By _birthdayMonthDropDown = By.Name("birthday_month");
        internal static By _birthdayMonthValue = By.XPath($"//*[@id='month']/option[{_month}]");
        internal static By _birthdayDayDropDown = By.Name("birthday_day");
        internal static By _birthdayDayValue = By.XPath($"//*[@id='day']/option[{_day}]");
        internal static By _birthdayYearDropDown = By.Name("birthday_year");
        internal static By _birthdayYearValue = By.XPath($"//*[@id='year']/option[{_year}])");
        internal static By _maleGenderButton = By.XPath("/html/body/div[3]/div[2]/div/div/div[2]/div/div/div[1]/form/div[1]/div[7]/span/span[2]/label");
        internal static By _femaleGenderButton = By.XPath("/html/body/div[3]/div[2]/div/div/div[2]/div/div/div[1]/form/div[1]/div[7]/span/span[1]/label");
        internal static By _customGenderButton = By.XPath("/html/body/div[3]/div[2]/div/div/div[2]/div/div/div[1]/form/div[1]/div[7]/span/span[3]/label");
        internal static By _customGenderInputButton = By.Name("custom_gender");
        internal static By _preferredPronounDropDown= By.Name("preferred_pronoun");
        internal static By _preferredPronounValue = By.Name($"/html/body/div[3]/div[2]/div/div/div[2]/div/div/div[1]/form/div[1]/div[8]/div[1]/select/option[{_pronoun}]");
        internal static By _submitButton = By.Name("websubmit");
        internal static By _codeConfirmInputButton = By.Name("code");
        internal static By _codeConfirmButton = By.Id("u_f_f_tW");
        internal static By _okButton = By.XPath("//*[@id='facebook']/body/div[4]/div[2]/div/div/div/div[3]/div/a");
        internal static By _accountName = By.CssSelector("#mount_0_0_bM > div > div:nth-child(1) > div > div.rq0escxv.l9j0dhe7.du4w35lb > div > div > div.j83agx80.cbu4d94t.d6urw2fd.dp1hu0rb.l9j0dhe7.du4w35lb > div.rq0escxv.l9j0dhe7.du4w35lb.j83agx80.pfnyh3mw.taijpn5t.gs1a9yip.owycx6da.btwxx1t3.dp1hu0rb.p01isnhg > div > div.rq0escxv.lpgh02oy.du4w35lb.o387gat7.qbu88020.pad24vr5.rirtxc74.dp1hu0rb.fer614ym.ni8dbmo4.stjgntxs.rek2kq2y.be9z9djy.bx45vsiw > div > div > div.j83agx80.cbu4d94t.buofh1pr.l9j0dhe7 > div > div > div.buofh1pr > ul > li > div > a > div.ow4ym5g4.auili1gw.rq0escxv.j83agx80.buofh1pr.g5gj957u.i1fnvgqd.oygrvhab.cxmmr5t8.hcukyx3x.kvgmc6g5.nnctdnn4.hpfvmrgz.qt6c0cv9.jb3vyjys.l9j0dhe7.du4w35lb.bp9cbjyn.btwxx1t3.dflh9lhu.scb9dxdr > div.ow4ym5g4.auili1gw.rq0escxv.j83agx80.buofh1pr.g5gj957u.i1fnvgqd.oygrvhab.cxmmr5t8.hcukyx3x.kvgmc6g5.tgvbjcpo.hpfvmrgz.qt6c0cv9.rz4wbd8a.a8nywdso.jb3vyjys.du4w35lb.bp9cbjyn.btwxx1t3.l9j0dhe7 > div > div > div > div > span > span");
        internal static By _errorMessage = By.XPath("/html/body/div[11]/div/div/div");

        internal static void ClickRegistartionButton() => ChromeBrowser.GetDriver().FindElement(_createNewAccountButton).Click();
        internal static void SetFirstName(string firstname) =>
            ChromeBrowser.GetDriver().FindElement(_firstNameInputButton).SendKeys(firstname);
        internal static void SetLastName(string lasttname) =>
            ChromeBrowser.GetDriver().FindElement(_lastNameInputButton).SendKeys(lasttname);
        internal static void SetEmail(string email) =>
            ChromeBrowser.GetDriver().FindElement(_emailInputButton).SendKeys(email);
        internal static void SetReEmail(string email) =>
            ChromeBrowser.GetDriver().FindElement(_reEnterEmailInputButton).SendKeys(email);
        internal static void SetPassword(string password) =>
            ChromeBrowser.GetDriver().FindElement(_passwordInputButton).SendKeys(password);
        //internal static void BirthdayDay() => ChromeBrowser.GetDriver().FindElement(_birthdayDayDropDown);

        internal static void ClickAndSetBirthdayDay(int day)
        {
            IWebElement element = ChromeBrowser.GetDriver().FindElement(_birthdayDayDropDown);
            SelectElement select = new SelectElement(element);
            select.SelectByValue(day.ToString());
        }

        internal static void ClickAndSetBirthdayMonth(int month)
        {
            IWebElement element = ChromeBrowser.GetDriver().FindElement(_birthdayMonthDropDown);
            SelectElement select = new SelectElement(element);
            select.SelectByValue(month.ToString());
        }

        internal static void ClickAndSetBirthdayYear(int year)
        {
            IWebElement element = ChromeBrowser.GetDriver().FindElement(_birthdayYearDropDown);
            SelectElement select = new SelectElement(element);
            select.SelectByValue(year.ToString());
        }
        internal static void ClickAndSetPronoun(int pronoun)
        {
            IWebElement element = ChromeBrowser.GetDriver().FindElement(_preferredPronounDropDown);
            SelectElement select = new SelectElement(element);
            select.SelectByValue(pronoun.ToString());
        }
        internal static void SetGenderDescription(string gender)
        {
            ChromeBrowser.GetDriver().FindElement(_customGenderInputButton).SendKeys(gender);
        }

        internal static void SetMaleGender() => ChromeBrowser.GetDriver().FindElement(_maleGenderButton);
        internal static void SetFemaleGender() => ChromeBrowser.GetDriver().FindElement(_femaleGenderButton);
        internal static void SetCustomGender() => ChromeBrowser.GetDriver().FindElement(_customGenderButton);
        internal static void ClickSubmitButton() => ChromeBrowser.GetDriver().FindElement(_submitButton).Click();
        internal static void SetCode()
        {
            var code = TempMailApi.ApiRequest();
            ChromeBrowser.GetDriver().FindElement(_codeConfirmInputButton).SendKeys(code);
        }

        internal static void ClickContinueButtoon() => ChromeBrowser.GetDriver().FindElement(_codeConfirmButton).Click();
        internal static bool ClickOk()
        {
            try
            {
                ChromeBrowser.GetDriver().FindElement(_okButton);
            }
            catch
            {
                return false;
            }
            return true;
        }
        internal static string FindName() => ChromeBrowser.GetDriver().FindElement(_accountName).Text;
        internal static string GetErrorMessage() => ChromeBrowser.GetDriver().FindElement(_errorMessage).Text;


    }
}
