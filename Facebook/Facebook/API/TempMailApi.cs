﻿using Newtonsoft.Json;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Facebook.TempMail
{
    internal static class TempMailApi
    {
        internal static string ApiRequest()
        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://cryptogmail.com/api/emails?inbox=bunotahan.neramuve@vintomaper.com");
            request.Method = "Get";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string myResponse = "";
            using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream()))
            {
                myResponse = sr.ReadToEnd();
            }
            var model =JsonConvert.DeserializeObject<Rootobject>(myResponse);
            var subject = model.data[0].subject;
            var code = subject.Substring(0, 6);
            return code;

        }
    }
}
