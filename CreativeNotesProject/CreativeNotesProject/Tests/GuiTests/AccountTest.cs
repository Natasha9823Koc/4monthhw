﻿using CreativeNotes.PageObject;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CreativeNotes.GuiTests
{
   public class AccountTest
    {
        IWebDriver driver;
        [SetUp]
        public void Setup()
        {

            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://3.144.199.135/login");
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
         
        }
        [TestCase("Ivan", "qwert12345", "Natasha", "Petrova","Kharkiv","18")]
       public void CheckChangingAdditionalInfoWithValidData(string login, string password, string newname, string newlastname, string newcity, string newage)
        {
            AuthorizationPageObgect authorizationPageObgect = new AuthorizationPageObgect(driver);
            authorizationPageObgect.Authorization(login, password);
            Thread.Sleep(3000);
            MainMenuPageObject mainMenuPageObject = new MainMenuPageObject(driver);

            mainMenuPageObject.GetAccountButton();
            Thread.Sleep(3000);
            AccountPageObject accountPageObject = new AccountPageObject(driver);
            accountPageObject.ChangeAdditionalData(newname, newlastname, newcity, newage);
            var actualName = accountPageObject.GetName();
            var actualLastName = accountPageObject.GetLastName();
            var actualCity = accountPageObject.GetCity();
            var actualAge = accountPageObject.GetAge();

            Assert.AreEqual(actualName, newname, "Sorry...Wrong information");
            Assert.AreEqual(actualLastName, newlastname, "Sorry...Wrong information");
            Assert.AreEqual(actualCity, newcity, "Sorry...Wrong information");
            Assert.AreEqual(actualAge, newage, "Sorry...Wrong information");
        }
    }
}
