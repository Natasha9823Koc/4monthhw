﻿using CreativeNotes.APITests;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CreativeNotes.Tests.ApiTests
{
    class RegistrationTestsAPI
    {
        [TestCase("Charlie", "Qwert12345")]
        [TestCase("Thomas", "Sdgtre12345")]
        [TestCase("Jacob", "Dugusaa6754")]
        [TestCase("Alfie", "Кugusaa8764")]
        [TestCase("Riley", "Boeoq7654")]



        public void CheckRegistrationWithValidData(string login, string password)
        {
            var statusCode = RegistrationAPI.RegistrationPostRequest(login, password);
            Assert.AreEqual(HttpStatusCode.Created, statusCode);
        }
    }
}
