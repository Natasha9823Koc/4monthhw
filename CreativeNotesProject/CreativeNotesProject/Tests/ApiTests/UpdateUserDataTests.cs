﻿using CreativeNotes.APITests;
using CreativeNotes.GuiTests;
using CreativeNotes.PageObject;
using CreativeNotesProject.API.Request;
using NUnit.Framework;
using NUnitTestProject1.API.Requests;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;

namespace CreativeNotesProject.Tests.ApiTests
{
    class UpdateUserDataTests
    {


            IWebDriver driver;
            [TestCase("Ivan", "qwert12345", "Izolda")]
            public void CheckRegistrationWithValidData(string login, string password,string name)
            {
                var parameters = new Dictionary<string, object>
            {
                {"age","20"},
                {"city","Kharkiv"},
                {"img", "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQA"},
                {"lastName","Petrov"},
                {"name",name}
            };
                var token = Auth.AuthPost(login, password);
                UpdateUserData.UpdatePuttRequest(token, parameters);
                AccountTest accountTest = new AccountTest();
                driver = new ChromeDriver();
                driver.Navigate().GoToUrl("http://3.133.100.46/login");
                driver.Manage().Window.Maximize();
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                AuthorizationPageObgect authorizationPageObgect = new AuthorizationPageObgect(driver);
                authorizationPageObgect.Authorization(login, password);
                Thread.Sleep(3000);
                MainMenuPageObject mainMenuPageObject = new MainMenuPageObject(driver);
            mainMenuPageObject.GetAccountButton();
                AccountPageObject accountPageObject = new AccountPageObject(driver);
                var actualName = accountPageObject.GetName();
                var actualLastName = accountPageObject.GetLastName();
                var actualCity = accountPageObject.GetCity();
                Assert.AreEqual(name, actualName);
            }
        
    }
}
//{
//name: string;
//lastName: string;
//img: string(Base64);
//city: string;
//age: string;
//}