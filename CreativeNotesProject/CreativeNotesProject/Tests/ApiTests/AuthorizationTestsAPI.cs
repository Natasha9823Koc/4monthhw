﻿using CreativeNotes.APITests;
using CreativeNotesProject.API;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace CreativeNotesProject.ApiTests
{
    class AuthorizationTestsAPI
    {
        [Test]
        public void CheckAuthorizationWithValidData()
        {
            var statusCode = AuthorizationApi.AuthorizationPostRequest();
            Assert.AreEqual(HttpStatusCode.OK, statusCode);
        }
    }
}
