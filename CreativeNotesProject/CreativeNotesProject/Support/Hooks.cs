﻿using NUnit.Framework;

namespace NUnitTestProject1.Support
{
    public class Hooks
    {
        [SetUp]
        public void SetUp()
        {
            ChromeBrowser.GetDriver().Navigate().GoToUrl("https://newbookmodels.com/auth/signin");
        }

        [TearDown]
        public void TearDown()
        {
            ChromeBrowser.CleanDriver();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            ChromeBrowser.CloseDriver();
        }
    }
}