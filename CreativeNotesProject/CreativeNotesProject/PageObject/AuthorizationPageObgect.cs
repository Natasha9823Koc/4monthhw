﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreativeNotes.PageObject
{
    class AuthorizationPageObgect
    {
        private IWebDriver webdriver;
        private readonly By _logInInputButton = By.XPath("//input[@type='text']");
        private readonly By _passwordInputButton = By.XPath("//input[@type='password']");
        private readonly By _logInButton = By.XPath("//*[@id='root']/div[2]/button");
        private readonly By _notValidEmailErrorMassage = By.CssSelector("#root > div.sc-iBPTik.gaYDfa > button");

        public string GetUrl() => webdriver.Url;
        IWebElement GetLoginField() => webdriver.FindElement(_logInInputButton);
        IWebElement GetPasswordField() => webdriver.FindElement(_passwordInputButton);
        IWebElement GetLoginButton() => webdriver.FindElement(_logInButton);
        public string GetNotValidEmailError() => webdriver.FindElement(_notValidEmailErrorMassage).Text;




        public AuthorizationPageObgect(IWebDriver webdriver)
        {
            this.webdriver = webdriver;
        }
        public MainMenuPageObject Authorization(string login, string password)
        {
            var loginField = GetLoginField();
            loginField.SendKeys(login);
            var passwordField = GetPasswordField();
            passwordField.SendKeys(password);
            var loginButton = GetLoginButton();
            loginButton.Click();
            return new MainMenuPageObject(webdriver);
        }
    }
}
