﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreativeNotes.PageObject
{
    class MainMenuPageObject
    {
        private IWebDriver webdriver;
        private readonly By _accountButton = By.XPath("/html/body/div[1]/div[1]/div/div/div/div");

        public MainMenuPageObject(IWebDriver webdriver)
        {
            this.webdriver = webdriver;
        }
        public void GetAccountButton()
        {
            Actions actions = new Actions(webdriver);
            WebElement elementLocator = (WebElement)webdriver.FindElement(_accountButton);
            actions.Click(elementLocator).Perform();
        }
    }
}
