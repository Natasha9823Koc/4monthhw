﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreativeNotes.PageObject
{
    class AccountPageObject
    {
        private IWebDriver webdriver;
        private readonly By _additionalInfoButton = By.Id("change-info");
        private readonly By _changeAdditionalInfoButton = By.XPath("//img[@src= 'assets/img/crest.png']");
        private readonly By _nameInputButton = By.CssSelector(" div.sc-lmoMya.fBRqpo > span:nth-child(1) > div > input");
        private readonly By _lastNameInputButton = By.CssSelector(" div.sc-lmoMya.fBRqpo > span:nth-child(2) > div > input");
        private readonly By _cityInputButton = By.CssSelector(" div.sc-lmoMya.fBRqpo > span:nth-child(3) > div > input");
        private readonly By _ageInputButton = By.CssSelector(" div.sc-lmoMya.fBRqpo > span:nth-child(4) > div > input");
        private readonly By _addFileButton = By.XPath("//label[@for='upload']");
        private readonly By _submitButton = By.CssSelector(" div.sc-dmlqKv.hSQiWD > div > button");
        private readonly By _changePasswordButton = By.Id("change-password");
        private readonly By _currentPasswordInputButton = By.XPath("//*[@id='root']/div[2]/div[2]/div/div[1]/input");
        private readonly By _newPasswordInputButton = By.XPath("//*[@id='root']/div[2]/div[2]/div/div[2]/input");
        private readonly By _nameField = By.CssSelector(" div.sc-lmoMya.fBRqpo > span:nth-child(1) > p:nth-child(2)");
        private readonly By _lastnameField = By.CssSelector("div.sc-lmoMya.fBRqpo > span:nth-child(2) > p:nth-child(2)");
        private readonly By _cityField = By.CssSelector("div.sc-lmoMya.fBRqpo > span:nth-child(3) > p:nth-child(2)");
        private readonly By _ageField = By.CssSelector("div.sc-lmoMya.fBRqpo > span:nth-child(4) > p:nth-child(2)");







        public AccountPageObject(IWebDriver webdriver)
        {
            this.webdriver = webdriver;
        }

        public string GetUrl() => webdriver.Url;
        IWebElement GetAdditionalInfoButton() => webdriver.FindElement(_additionalInfoButton);
        IWebElement GetChangeAdditionalInfoButton() => webdriver.FindElement(_changeAdditionalInfoButton);
        IWebElement GetNameField() => webdriver.FindElement(_nameInputButton);
        IWebElement GetLastNameField() => webdriver.FindElement(_lastNameInputButton);
        IWebElement GetCityField() => webdriver.FindElement(_cityInputButton);
        IWebElement GetAgeField() => webdriver.FindElement(_ageInputButton);
        IWebElement GetFileUploud() => webdriver.FindElement(_addFileButton);
        IWebElement GetChangePasswordButton() => webdriver.FindElement(_changePasswordButton);

        IWebElement GetCurrentPasswordField() => webdriver.FindElement(_currentPasswordInputButton);
        IWebElement GetNewPasswordField() => webdriver.FindElement(_newPasswordInputButton);

        IWebElement GetSubmitButton() => webdriver.FindElement(_submitButton);
        public string GetName() => webdriver.FindElement(_nameField).Text;
        public string GetLastName() => webdriver.FindElement(_lastnameField).Text;
        public string GetCity() => webdriver.FindElement(_cityField).Text;
        public string GetAge() => webdriver.FindElement(_ageField).Text;





        public AccountPageObject ChangeAdditionalData(string newname, string newlastname,string newcity, string newage)
        {
            var additionalInfoButton = GetAdditionalInfoButton();
            additionalInfoButton.Click();
            var changeAdditionalInfo = GetChangeAdditionalInfoButton();
            changeAdditionalInfo.Click();
            var name = GetNameField();
            name.Clear();
            name.SendKeys(newname);
            var lastname = GetLastNameField();
            lastname.Clear();
            lastname.SendKeys(newlastname);;
            var city = GetCityField();
            city.Clear();
            city.SendKeys(newcity);
            var age = GetAgeField();
            age.Clear();
            age.SendKeys(newage);
            var submit = GetSubmitButton();
            submit.Click();
            return new AccountPageObject(webdriver);
        }
        public AccountPageObject UploadPhoto(string path)
        {
            var fileUpload = GetFileUploud();
            fileUpload.Click();
            fileUpload.SendKeys(path);
            var submit = GetSubmitButton();
            submit.Click();
            return new AccountPageObject(webdriver);
        }
        public AccountPageObject ChangePassword(string currentpass,string newpass)
        {
            var changePassword = GetChangePasswordButton();
            changePassword.Click();
            var currentPassword = GetCurrentPasswordField();
            currentPassword.SendKeys(currentpass);
            var newPassword = GetNewPasswordField();
            newPassword.SendKeys(newpass);
            var submit = GetSubmitButton();
            submit.Click();
            return new AccountPageObject(webdriver);
        }














    }
}
