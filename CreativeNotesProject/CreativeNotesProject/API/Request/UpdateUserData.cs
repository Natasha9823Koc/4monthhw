﻿using NUnitTestProject1.Support;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace CreativeNotesProject.API.Request
{
    class UpdateUserData
    {
        internal static string UpdatePuttRequest(string token, Dictionary<string, object> parameters)
        {
            ApiRequest.ConfigureApiRequest("http://3.144.199.135/api/change_user_data");
            ApiRequest.SetRequestType(Method.PUT);
            var headers = new Dictionary<string, string>
            {
                {"Authorization", token}
            };
            ApiRequest.SetData(parameters, headers);
            var response = ApiRequest.SendRequest();
            return response.Content;
        }
    }
}
