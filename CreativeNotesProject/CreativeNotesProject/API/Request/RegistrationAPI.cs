﻿using NUnitTestProject1.Support;
using System.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace CreativeNotes.APITests
{
    class RegistrationAPI
    {
        internal static HttpStatusCode RegistrationPostRequest(string login,string password)
        {
            ApiRequest.ConfigureApiRequest("http://3.144.199.135/api/registration");
            ApiRequest.SetRequestType(Method.POST);
            var body = new Dictionary<string, object>
            {
                {"login",login},
                {"password",password},
            };
            ApiRequest.SetData(body);
            var response = ApiRequest.SendRequest();
            return response.StatusCode;
        }
    }
}
