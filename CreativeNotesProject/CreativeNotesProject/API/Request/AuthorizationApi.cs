﻿using NUnitTestProject1.Support;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace CreativeNotesProject.API
{
    class AuthorizationApi
    {
        internal static HttpStatusCode AuthorizationPostRequest()
        {
            ApiRequest.ConfigureApiRequest("http://3.144.199.135/api/authorization");
            ApiRequest.SetRequestType(Method.POST);
            var body = new Dictionary<string, object>
            {
                {"login","admin"},
                {"password","admin123"},
            };
            ApiRequest.SetData(body);
            var response = ApiRequest.SendRequest();
            return response.StatusCode;
        }
    }
}
