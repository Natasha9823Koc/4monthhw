﻿using System.Collections.Generic;
using Newtonsoft.Json;
using NUnitTestProject1.API;
using NUnitTestProject1.Support;
using RestSharp;

namespace NUnitTestProject1.API.Requests
{
    internal static class Auth
    {
        internal static string AuthPost(string email, string password)
        {
            ApiRequest.ConfigureApiRequest("http://3.144.199.135/api/authorization");
            ApiRequest.SetRequestType(Method.POST);
            var parameters = new Dictionary<string, object>
            {
                {"login", email},
                {"password", password}
            };
            ApiRequest.SetData(parameters);

            var response = ApiRequest.SendRequest();
            return response.Content;
        }
    }
}
