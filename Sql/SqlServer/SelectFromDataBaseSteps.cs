﻿using NUnit.Framework;
using Sql;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SqlServer
{
    [Binding]
    public class SelectFromDataBaseSteps
    {
        private int countOfRows;
        private List<PersonModel> BeforeInsert = new List<PersonModel>();
        private List<PersonModel> peopleAfterInsert = new List<PersonModel>();

        private string command;
        [Given(@"Count of rows in table '(.*)' in '(.*)' catalog was memorized")]
        public void GivenCountOfRowsInTableInCatalogWasMemorized(string p0, string p1)
        {
            countOfRows = BeforeInsert.Count;
        }
        
        [Given(@"Data of rows in table '(.*)' in '(.*)' catalog was memorized")]
        public void GivenDataOfRowsInTableInCatalogWasMemorized(string p0, string p1)
        {
            command = "Select * From [Persons]";
            BeforeInsert = SqlRequests.Execute(command);
        }

        [Given(@"Rows was inserted to table '(.*)' in '(.*)' catalog")]
        public void GivenRowsWasInsertedToTableInCatalog(string tableBd, string p1, Table table)
        {
            var model = table.CreateInstance<PersonModel>();
            var id = countOfRows + 1;
            var firstName = model.FirstName;
            var lastName = model.LastName;
            var age = model.Age;
            var city = model.City;

            command = $"insert into {tableBd} (FirstName, LastName, Age, City) values ('{firstName}','{lastName}',{age},'{city}')";
            SqlRequests.Execute(command);
        }

        [When(@"I select '(.*)' values from Table '(.*)' in '(.*)' catalog")]
        public void WhenISelectValuesFromTableInCatalog(string scriptOperator, string p1, string p2)
        {
            command =  $"Select {scriptOperator} From [Persons]";
        }

        [When(@"I send sql script")]
        public void WhenISendSqlScript()
        {
            peopleAfterInsert=SqlRequests.Execute(command);
        }
        
        [When(@"I select all values from Table '(.*)' in '(.*)' catalog")]
        public void WhenISelectAllValuesFromTableInCatalog(string p0, string p1, Table table)
        {
            var parametr = table.Rows[0][0];
            var value = table.Rows[0][1];
            command = $"Select * From [Persons] Where {parametr}='{value}'";

        }

        [Then(@"Memorized rows was received")]
        public void ThenMemorizedRowsWasReceived()
        {
            foreach (var person in BeforeInsert)
            {
                foreach (var newperson in peopleAfterInsert)
                {
                    
                    Assert.AreEqual(person.FirstName, newperson.FirstName);
                    Assert.AreEqual(person.LastName, newperson.LastName);
                    Assert.AreEqual(person.Age, newperson.Age);
                    Assert.AreEqual(person.City, newperson.City);
                }
            }
        }
        
        [Then(@"Memorized rows count was increased with '(.*)'")]
        public void ThenMemorizedRowsCountWasIncreasedWith(int num)
        {
            var increasedRows = peopleAfterInsert.Count - countOfRows;
            Assert.AreEqual(num, increasedRows);
        }
        
        [Then(@"Recived table has rows")]
        public void ThenRecivedTableHasRows(Table table)
        {
            PersonModel person = new PersonModel();
            var model = table.CreateInstance<PersonModel>();
            person.Id = countOfRows + 1;
            person.FirstName = model.FirstName;
            person.LastName = model.LastName;
            person.Age = model.Age;
            person.City = model.City;

            Assert.AreEqual(peopleAfterInsert[peopleAfterInsert.Count-1].FirstName,person.FirstName);
            Assert.AreEqual(peopleAfterInsert[peopleAfterInsert.Count - 1].LastName, person.LastName);
            Assert.AreEqual(peopleAfterInsert[peopleAfterInsert.Count - 1].Age, person.Age);
            Assert.AreEqual(peopleAfterInsert[peopleAfterInsert.Count - 1].City, person.City);

        }

        [Then(@"Recived table has rows with values")]
        public void ThenRecivedTableHasRowsWithValues(Table table)
        {

            PersonModel person = new PersonModel();
            var model = table.CreateInstance<PersonModel>();
            person.Id = countOfRows + 1;
            person.FirstName = model.FirstName;
            person.LastName = model.LastName;
            person.Age = model.Age;
            person.City = model.City;
            foreach (var per in peopleAfterInsert)
            {
                Assert.AreEqual(per.FirstName, person.FirstName);
                Assert.AreEqual(per.LastName, person.LastName);
                Assert.AreEqual(per.Age, person.Age);
                Assert.AreEqual(per.City, person.City);
            }

        }
    }
}
