﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SqlServer
{
    class PersonModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string City { get; set; }      
    }
}
