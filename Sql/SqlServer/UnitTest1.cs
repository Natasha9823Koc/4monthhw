using NUnit.Framework;
using Sql;
using System.Collections.Generic;

namespace SqlServer
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [TestCase("FirstName", "������")]
        [TestCase("LastName", "��������")]
        [TestCase("City", "���������")]


        //[TestCase("Select count(*) From [Persons]")]
        //[TestCase("Select * From [Persons] Where FirstName='���������'")]
        //[TestCase("Select max(Age) From [Persons]")]
        //[TestCase("FirstName", "������")]
        //public void CheckSelectCommandsWithoutParametrs()
        //{
        //    string command

        //}
        public void CheckSelectCommandsWithStringParametrs(string parametr, string stringvalue)
        {
            string command = $"Select * From [Persons] Where {parametr}='{stringvalue}'";
           List<PersonModel> people= SqlRequests.Execute(command);
            foreach (var person in people)
            {
                switch (parametr)
                {
                    case "FirstName":
                        Assert.AreEqual(person.FirstName, stringvalue);
                        break;
                    case "LastName":
                        Assert.AreEqual(person.LastName, stringvalue);
                        break;
                    case "City":
                        Assert.AreEqual(person.City, stringvalue);
                        break;
                }
            }
        }
        [TestCase("Age", 18)]
        [TestCase("Id", 15)]
        public void CheckSelectCommandsWithIntParametrs(string parametr, int value)
        {
            string command = $"Select * From [Persons] Where {parametr}='{value}'";
            List<PersonModel> people = SqlRequests.Execute(command);
            foreach (var person in people)
            {
                switch (parametr)
                {
                    case "Age":
                        Assert.AreEqual(person.Age, value);
                        break;                    
                    case "Id":
                        Assert.AreEqual(person.Id, value);
                        break;
                }
            }
        }


    }
}