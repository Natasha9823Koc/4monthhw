﻿@DataBase
Feature: Select from DataBase
	
Scenario: Select all values from table 
Given Data of rows in table 'Persons' in 'DfLessonDb' catalog was memorized
And Count of rows in table 'Persons' in 'DfLessonDb' catalog was memorized
And Rows was inserted to table 'Persons' in 'DfLessonDb' catalog
| FirstName | LastName | Age | City   |
| Александр | Пушкин   | 23  | Москва |
When I select '*' values from Table 'Persons' in 'DfLessonDb' catalog
And I send sql script
Then Memorized rows was received
And Memorized rows count was increased with '1'
And Recived table has rows
| FirstName | LastName | Age | City   |
| Александр | Пушкин   | 23  | Москва |

Scenario Outline: Select specific values from table 
Given Data of rows in table 'Persons' in 'DfLessonDb' catalog was memorized
And Count of rows in table 'Persons' in 'DfLessonDb' catalog was memorized
When I select all values from Table 'Persons' in 'DfLessonDb' catalog
	| Parametr   | Value   |
	| <parametr> | <value> |
And I send sql script
Then Recived table has rows with values
	| Parametr   | Value   |
	| <parametr> | <value> |
Examples: 
	| parametr  | value   |
	| Id        | 5       |
	| FirstName | Сергей  |
	| LastName  | Иванов  |
	| Age       | 25      |
	| City      | Харьков |
