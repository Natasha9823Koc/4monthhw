﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using SqlServer;
using System.Data;

namespace Sql
{
    static class SqlRequests
    {
        private static readonly string connectionString = @"Data Source=WIN-P0DQU47J2IT\SQLEXPRESS;Initial Catalog=DfLessonDb;Integrated Security=True";/*ConfigurationManager.ConnectionStrings["DfLessonDb"].ConnectionString;*/
        private static SqlConnection sqlConnection = null;
        static SqlCommand sqlCommand = null;

        public static List<PersonModel> Execute(string command)
        {
            PersonModel person = new PersonModel();
            List<PersonModel> people = new List<PersonModel>();
            sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            //SqlDataReader sqlDataReader = null;
            sqlCommand = new SqlCommand(command, sqlConnection);
            switch (command.Split(' ')[0].ToLower())
            {
                case "select":
                    people = Select(sqlCommand);
                    break;
                case "insert":
                    SqlDataReader sqlDataReader = null;
                    sqlDataReader = sqlCommand.ExecuteReader();
                    break;
            }
            return people;
        }
        public static List<PersonModel> Select(SqlCommand sqlCommand)
        {
            List<PersonModel> people = new List<PersonModel>();
            SqlDataReader sqlDataReader = null;
            sqlDataReader = sqlCommand.ExecuteReader();
            while (sqlDataReader.Read())
            {

                PersonModel person = new PersonModel();
                person.Id = (int)sqlDataReader["Id"]; person.FirstName = (string)sqlDataReader["FirstName"];
                person.LastName = (string)sqlDataReader["LastName"]; person.Age = (int)sqlDataReader["Age"];
                person.City = (string)sqlDataReader["City"];
                people.Add(person);
            }
            if (sqlDataReader != null)
            {
                sqlDataReader.Close();
            }
            return people;
        }

    }
}
